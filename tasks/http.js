
module.exports = function(grunt) {
  grunt.registerTask('http', function() {
    setTimeout(function() {
      var cp = require('child_process'),
          task = cp.spawn(process.execPath, [ 'index.js' ], {
            cwd: process.cwd(),
            env: process.env
          });

      console.log('\nCards: starting...');

      task.stdout.pipe(process.stdout);
      task.stderr.pipe(process.stderr);
      task.on('exit', function() {
        process.exit();
      });
    }, 500);
  });
}

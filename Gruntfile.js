'use strict';

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json')
  });
  grunt.loadTasks('./tasks');

  require('load-grunt-tasks')(grunt);

  grunt.config.set('copy', {
    vendorJS: {
      cwd: 'vendor',
      expand: true,
      dest: 'build/js',
      src: [ 'angular/**', 'async/lib/**', 'jquery/**' ]
    },
    vendorCSS: {
      cwd: 'vendor',
      expand: true,
      dest: 'build/css',
      src: [ ]
    },
    vendorBootstrapFonts: {
      dest: 'build',
      expand: true,
      cwd: 'vendor/bootstrap/assets',
      src: [
        'fonts/**'
      ]
    },

    partials: {
      cwd: 'application',
      expand: true,
      dest: 'build',
      src: [ 'partials/**' ]
    },
    js: {
      cwd: 'application',
      expand: true,
      dest: 'build',
      src: [ 'js/**' ]
    }
  });

  grunt.config.set('sass', {
    dist: {
      options: {
        loadPath: [
          'application/styles',
          'vendor/bootstrap/assets/stylesheets'
        ]
      },
      files: {
        'build/styles/app.css': 'application/styles/app.sass'
      }
    },
  });

  var htmlBuildOptions = {
    logOptions: true,
    relative: true,
    beautify: true,
    prefix: '/',

    scripts: {
      bundle: [
        'build/js/async/lib/async.js',
        'build/js/jquery/*.js',
        'build/js/angular/angular.js',
        'build/js/angular/angular-*.js',
        'build/js/*.js',
        'build/js/controllers/*.js',
        'build/js/services/*.js',
        'build/js/directives/*.js'
      ]
    },
    styles: {
      bundle: 'build/styles/**/*.css'
    }
  };

  grunt.config.set('htmlbuild', {
    all: {
      src: 'application/*.html',
      dest: 'build',
      options: htmlBuildOptions
    }
  });

  grunt.config.set('watch', {
    scripts: {
      files: [ 'application/js/**/*' ],
      tasks: [ 'copy:js', 'htmlbuild' ]
    },
    styles: {
      files: [ 'application/styles/**/*' ],
      tasks: [ 'sass:dist' ]
    },

    html: {
      files: [ 'application/*.html' ],
      tasks: [ 'htmlbuild' ]
    },

    vendor: {
      files: [ 'vendor/**/**' ],
      tasks: [
        'copy:vendorJS',
        'copy:vendorCSS',
        'copy:vendorBootstrapFonts'
      ]
    },

    partials: {
      files: [ 'application/partials/**' ],
      tasks: [ 'copy:partials' ]
    }
  });

  grunt.registerTask('default', [
    'copy',
    'sass',
    'htmlbuild',
    'http',
    'watch'
  ]);
};

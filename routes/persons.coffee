express = require 'express'
Order   = require './cacheable/order'
Person  = require './cacheable/person'

module.exports = express.Router()

Person.remove({ });

module.exports.get '/:orderId', (req, res) ->
  Person.find { order: req.params.orderId }
    .then (persons) ->
      return res.json persons if persons.length > 0;

      Order.findById req.params.orderId
        .then (order) ->
          Person.updateLocalPersons order
        .then (persons) ->
          res.json persons

    .catch (err) ->
      res.status(500).json err.stack

module.exports.get '/:personId/info', (req, res) ->
  Person.findById req.params.personId
    .then (person) ->
      Person.loadDetails person
    .then (person) ->
      person.save()
    .then (person) ->
      res.json person

    .catch (err) ->
      res.status(500).json err.stack

module.exports.put '/:personId', (req, res) ->
  Person.findById req.params.personId
    .then (person) ->
      person.address      = req.body.address
      person.birthdate    = req.body.birthdate
      person.birthplace   = req.body.birthplace
      person.eduDocument  = req.body.eduDocument
      person.passport     = req.body.passport
      person.sex          = req.body.sex
      person.fio          = req.body.fio
      person.synchronized = false
      person.save()

    .then (person) ->
      res.json person

    .catch (err) ->
      res.status(500).json err.stack

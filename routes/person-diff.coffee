express = require 'express'
Person  = require './cacheable/person'
diff    = require './helpers/diff'

module.exports = express.Router()

module.exports.get '/', (req, res) ->
  Person.find({ synchronized: false }).limit(40)
    .then (persons) ->
      res.json persons

    .catch (err) ->
      res.status(500).json err.stack

module.exports.get '/:id', (req, res) ->
  localPerson = null;
  dbPerson    = null;

  Person.findById req.params.id
    .then (person) ->
      dbPerson    = person;
      localPerson = person.toJSON();
      Person.loadDetails person.toJSON()

    .then (edboPerson) ->
      diff localPerson, edboPerson

    .then (differences) ->
      localPerson.differences = differences;

      if differences.length == 0
        dbPerson.synchronized = true;
        dbPerson.save();

    .then ->
      res.json localPerson

    .catch (err) ->
      res.status(500).json err.stack

module.exports.get '/:id/reset', (req, res) ->
  Person.findById req.params.id
    .then (person) ->
      Person.loadDetails person

    .then (person) ->
      person.synchorinized = true;
      person.save();

    .then (person) ->
      person = person.toJSON();
      person.differences = [];
      res.json person

    .catch (err) ->
      res.status(500).json err.stack

module.exports.get '/:id/dismiss', (req, res) ->
  Person.findById req.params.id
    .then (person) ->
      person.synchoronized = true;
      person.save();

    .then (person) ->
      res.json person

    .catch (err) ->
      res.status(500).json err.stack

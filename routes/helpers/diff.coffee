defaultIgnorableProperties = [ 'order', '_id', '__v', 'PersonCodeU', 'Id_PersonRequest', 'synchronized' ];

module.exports = (lhs, rhs, prefix) ->
  prefix = if prefix then prefix + '.' else ''
  differences = []

  for key, value of rhs
    unless key in defaultIgnorableProperties
      difference = compare lhs[key], rhs[key], key

      if !difference
        continue;

      if difference.length > 0
        differences = differences.concat difference

      if difference.length == undefined
        differences.push key: prefix + key, diff: difference

  differences

compare = (lhs, rhs, key) ->
  if !lhs and !rhs
    return lhs: '', rhs: ''

  if lhs and (lhs.toString() == '[object Object]' or lhs.splice != undefined)
    return module.exports lhs, rhs or { }, key

  if rhs and (rhs.toString() == '[object Object]' or rhs.splice != undefined)
    return module.exports lhs or { }, rhs, key

  lhs = new Date(lhs).toUTCString() if lhs and (/^.+T.+Z$/.test(lhs) or lhs.toUTCString)
  rhs = new Date(rhs).toUTCString() if rhs and (/^.+T.+Z$/.test(rhs) or rhs.toUTCString)

  if lhs != rhs
    lhs: lhs or '', rhs: rhs or ''

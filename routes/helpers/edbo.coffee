EDBO = require 'node-edbo'

delete process.env.http_proxy
delete process.env.HTTP_PROXY

EDBO.setGlobalConfig 'config/config.json'
module.exports = EDBO.session()

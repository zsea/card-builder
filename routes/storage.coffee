Promise = require 'bluebird'
express = require 'express'
crypto  = require 'crypto'
async   = require 'async'
Storage = require './cacheable/storage'

module.exports = express.Router()

module.exports.get '/:hash', (req, res) ->
  Storage
    .find { hash: req.params.hash }
    .deepPopulate 'persons persons.order'
    .then (items) ->
      res.json items[0]

    .catch (err) ->
      res.status(500).json err.stack

module.exports.post '/', (req, res) ->
  getHash()
    .then (hash) ->
      new Storage
        hash: hash,
        action:  req.body.action,
        orders:  req.body.orders,
        persons: req.body.persons
    .then (item) ->
      item.save()
    .then (item) ->
      res.json item

    .catch (err) ->
      res.status(500).json err.stack

getHashWithLength = () ->
  dateString = new Date().toJSON()
  iterations = 50 + Math.random() * 100

  hash = crypto.pbkdf2Sync dateString, 'zsea', iterations|0, 20
  Promise.resolve hash

getHash = ->
  result = undefined
  length = 2

  loopIteration = ->
    getHashWithLength()
      .then (hash) ->
        result = hash.toString('base64').replace(/[^\w\d]/g, '').substr(0, length++).toLowerCase()
        Storage.find { hash: result }
      .then (items) ->
        return loopIteration() if items.length
        result

  loopIteration()

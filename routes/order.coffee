express = require 'express'
Order   = require './cacheable/order'

module.exports = express.Router()

module.exports.get '/update', (req, res) ->
  Order.updateLocalOrders()
    .then ->
      res.json success: true
    .catch (err) ->
      res.status(500).json err.stack

module.exports.get '/:year', (req, res) ->
  Order.find { year: +req.params.year }
    .then (orders) ->
      res.json orders
    .catch (err) ->
      res.status(500).json err.stack

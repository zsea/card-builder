edbo = require '../helpers/edbo'

module.exports = edbo
  .then (connection) ->
    connection.Guides.UniversitiesGet
      UniversityKode: '',
      UniversityName: 'Запорізька державна інженерна академія'

  .then (result) ->
    result.dUniversities[0].UniversityKode

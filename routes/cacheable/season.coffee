edbo = require '../helpers/edbo'

seasons = { }

module.exports = (year) ->
  seasons[year] = seasons[year] or getSeasonFromEdbo(year)

getSeasonFromEdbo = (year) ->
  edbo
    .then (client) ->
      client.Person.PersonRequestSeasonsGet
        Id_PersonEducationForm: 1
        OnlyActive: 0
        Id_PersonRequestSeasons: 0
        ActualData: ''

    .then (response) ->
      response.dPersonRequestSeasons.filter (season) ->
        season.DateBeginPersonRequestSeason.getFullYear() == year

    .then (response) ->
      response[0].Id_PersonRequestSeasons if response[0]

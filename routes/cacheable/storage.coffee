Promise  = require 'bluebird'
mongoose = require 'mongoose'

deepPopulate = require 'mongoose-deep-populate'

schema = mongoose.Schema
  orders:  [ { type: mongoose.Schema.Types.ObjectId, ref: 'Order',  required: true } ],
  persons: [ { type: mongoose.Schema.Types.ObjectId, ref: 'Person', required: true } ],
  action:  { type: String, required: true },
  hash:    { type: String, required: true }

schema.plugin deepPopulate

module.exports = StorageModel = mongoose.model 'Storage', schema

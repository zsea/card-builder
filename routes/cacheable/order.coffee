Promise  = require 'bluebird'
mongoose = require 'mongoose'
edbo     = require '../helpers/edbo'

season     = require './season'
univercity = require './univercity'

orderSchema = mongoose.Schema
  Id_OrderOfEnrollment: Number,
  number: String,
  year:   Number,
  date:   Date,
  course: Number,
  qualification: String,
  form:          String,
  payment:       String,
  verified:      Boolean,
  enrolmentDate: Date

module.exports = OrderModel = mongoose.model 'Order', orderSchema

module.exports.updateLocalOrders = ->
  OrderModel.remove({ })
    .then ->
      Promise.props connection: edbo, season: season(new Date().getFullYear() - 1), univercity: univercity

    .then ({ connection, univercity, season }) ->
      connection.Guides.OrdersOfEnrollmentsGet2 {
        UniversityKode: univercity
        Id_PersonRequestSeasons: season
        Filters: ''
      }

    .then (response) ->
      response.dOrdersOfEnrollments2 = response.dOrdersOfEnrollments2 || []
      response.dOrdersOfEnrollments2.map (order) ->
        Id_OrderOfEnrollment: order.Id_OrderOfEnrollment,
        number: order.OrderOfEnrollmentNumber,
        date: order.OrderOfEnrollmentDate,
        course: order.OrdersOfEnrollmentsKorse,
        qualification: order.QualificationName.toLocaleLowerCase(),
        form: order.PersonEducationFormName.toLocaleLowerCase(),
        year: /\d+/.exec(order.AcademicYearName)[0];
        verified: order.IsVerified == 1,
        payment: if order.Id_PaymentType == 1 then 'бюджет' else 'контракт',
        enrolmentDate: order.OrdersOfEnrollmentsDatePriyoma

    .then (orders) ->
      Promise.all orders.map (order) -> new OrderModel(order).save();
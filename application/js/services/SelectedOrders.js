'use strict';

angular
  .module('ZSEACardBuilder.services')
  .service('SelectedOrders', SelectedOrdersConstructor);

function SelectedOrdersConstructor() {
  var orders = [ ];

  this.get = function() {
    return orders;
  };

  this.getIds = function() {
    return orders.map(function(order) {
      return order._id;
    });
  };

  this.set = function(collection) {
    orders = angular.copy(collection);
  };
}

'use strict';

angular
.module('ZSEACardBuilder.services')
.factory('PersonDiff', function($resource) {
  var defaults = {
    personId: '@_id'
  };

  return $resource('/api/diff/:personId', defaults, {
    reset: {
      method: 'GET',
      url: '/api/diff/:personId/reset'
    },
    dismiss: {
      method: 'GET',
      url: '/api/diff/:personId/dismiss'
    }
  });
});

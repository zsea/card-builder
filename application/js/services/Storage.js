'use strict';

angular
.module('ZSEACardBuilder.services')
.factory('Storage', function($resource) {
  return $resource('/api/storage/:hash', { }, { });
});

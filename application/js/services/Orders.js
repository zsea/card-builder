'use strict';

angular
.module('ZSEACardBuilder.services')
.factory('Orders', function($resource) {
  return $resource('/api/orders/:year', { }, {
    updateLocalOrders: {
      method: 'GET',
      url: '/api/orders/update'
    }
  });
});

'use strict';

angular
.module('ZSEACardBuilder.services')
.service('SelectedPersons', function() {
  var persons = [ ];

  this.get = function() {
    return persons;
  };

  this.getIds = function() {
    return persons.map(function(person) {
      return person._id;
    });
  };

  this.set = function(collection) {
    persons = angular.copy(collection);
  };

  this.add = function(collection) {
    collection.forEach(function(e) {
      persons.push(e);
    });
  };

  this.clear = function() {
    persons = [ ];
  };

  this.addOne = function(person) {
    persons.push(person);
  };

  this.removeOne = function(person) {
    var idx;
    persons.some(function(item, index) {
      if(item._id === person._id) {
        idx = index;
        return true;
      }
    })

    if(idx !== undefined) {
      persons.splice(idx, 1);
    }
  };

  this.toggle = function() {
    // TODO
  }
});

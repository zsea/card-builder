'use strict';

angular
.module('ZSEACardBuilder.services')
.factory('Persons', function($resource) {
  var defaults = {
    id: '@_id',
    personCode: '@personCode'
  };

  return $resource('/api/persons/:orderId', defaults, {
    info: {
      method: 'GET',
      url: '/api/persons/:id/info'
    },
    update: {
      method: 'PUT',
      url: '/api/persons/:id'
    }
  });
});

'use strict';

angular
.module('ZSEACardBuilder.controllers')
.controller('MainController', function($scope, $timeout, Storage, SelectedPersons, SelectedOrders) {

  $scope.isOrdersSelected = function() {
    return SelectedOrders.get().length > 0;
  };

  $scope.getOrders = function() {
    return SelectedOrders.get();
  };

  $scope.selectAll = function() {
    $scope.$broadcast('persons:selectall');
  };

  $scope.unselectAll = function() {
    $scope.$broadcast('persons:unselectall');
  };

  $scope.selectAnotherOrder = function() {
    SelectedPersons.set([]);
    SelectedOrders.set([]);

    $scope.pendingRequests = 0;
    $scope.totalRequests   = 0;
  };

  $scope.sortPersons = function() {
    $scope.$broadcast('persons:sort');
  };

  $scope.generateCards = function() {
    Storage.save({ }, {
      action:  'cards',
      orders:  SelectedOrders.getIds(),
      persons: SelectedPersons.getIds()
    },
    function(response) {
      $scope.lastHash = response.hash;
    });
  };

  $scope.generateExtractions = function() {
    Storage.save({ }, {
      action:  'extractions',
      orders:  SelectedOrders.getIds(),
      persons: SelectedPersons.getIds()
    },
    function(response) {
      $scope.lastHash = response.hash;
    });
  };

  $scope.generateInfoCards = function() {
    Storage.save({ }, {
      action:  'infocards',
      orders:  SelectedOrders.getIds(),
      persons: SelectedPersons.getIds()
    },
    function(response) {
      $scope.lastHash = response.hash;
    });
  };

  $scope.generateSpecCards = function() {
    Storage.save({ }, {
      action:  'speccards',
      orders:  SelectedOrders.getIds(),
      persons: SelectedPersons.getIds()
    },
    function(response) {
      $scope.lastHash = response.hash;
    });
  };

  $scope.pendingRequests = 0;
  $scope.totalRequests   = 0;
});

'use strict';

angular
.module('ZSEACardBuilder.controllers')
.controller('OrdersController', function($scope, Orders, SelectedOrders) {

  $scope.filters = { };
  $scope.orders  = Orders.query({ year: 2014 }, preProcessOrders);

  $scope.chooseAllFilters = chooseAllFilters;
  $scope.clearFilters     = clearFilters;
  $scope.confirmSelection = confirmSelection;

  $scope.select = function(order) {
    $scope.selected = order;
  };

  function preProcessOrders(orders) {
    orders.sort(function(lhs, rhs) {
      var qualification = lhs.qualification.localeCompare(rhs.qualification);
      if(qualification !== 0) {
        return qualification;
      }

      var form = lhs.form.localeCompare(rhs.form);
      if(form !== 0) {
        return form;
      }

      var payment = lhs.payment.localeCompare(rhs.payment);
      if(payment !== 0) {
        return payment;
      }
    });
  }

  function chooseAllFilters() {
    $scope.filters = {
      form: {
        D: true,
        Z: true
      },
      payment: {
        B: true,
        C: true
      },
      qualification: {
        B1: true,
        B2: true,
        B3: true,
        M:  true,
        S:  true
      }
    };
  }

  function clearFilters() {
    $scope.filters = { };
  }

  function confirmSelection($event) {
    if($scope.selected == -1) {
      SelectedOrders.set(getFilteredOrders($scope.orders));
    }
    else {
      SelectedOrders.set([ $scope.selected ]);
    }
  }

  function getFilteredOrders(orders) {
    var ret     = [ ],
        filters = [ ];
    for(var key in $scope.filters) {
      var crit = $scope.filters[key],
          obj  = { key: key };
      for(var k in crit) {
        if(crit[k] === true) {
          if(k == "D") {
            obj.val = "денна";
          }
          else if(k == "Z") {
            obj.val = "заочна";
          }
          else if(k == "B") {
            obj.val = "бюджет";
          }
          else if(k == "C") {
            obj.val = "контракт";
          }
          else if(k == "B1") {
            obj.val = "бакалавр";
          }
          else if(k == "B2") {
            obj.val = "бакалавр (з нормативним терміном навчання, на 2 курс)";
          }
          else if(k == "B3") {
            obj.val = "бакалавр (з нормативним терміном навчання, на 3 курс)";
          }
          else if(k == "M") {
            obj.val = "магістр";
          }
          else if(k == "S") {
            obj.val = "спеціаліст";
          }
          filters.push(obj);
        }
      }
    }
    var checkOrder = function(order) {
      for(var i in filters) {
        if(order[filters[i].key] !== filters[i].val) {
          return false;
        }
      }
      return true;
    };

    orders.forEach(function(e) {
      if(checkOrder(e))
        ret.push(e);
    });
    return ret;
  }
});

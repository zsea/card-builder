'use strict';

angular
.module('ZSEACardBuilder.controllers')
.controller('DiffController', function($scope, PersonDiff) {
  $scope.persons = PersonDiff.query();

  $scope.checkPerson = checkPerson;
  $scope.recheck = recheck;
  $scope.reset   = reset;
  $scope.dismiss = dismiss;

  function checkPerson(person) {
    person.$get();
  }

  function recheck(person) {
    person.$get();
  }

  function reset(person) {
    person.$reset();
  }

  function dismiss(person) {
    person.$dismiss().then(function() {
      var index = $scope.persons.indexOf(person);
      $scope.persons.splice(index, 1);
    });
  }
});

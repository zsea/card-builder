'use strict';

angular
.module('ZSEACardBuilder.controllers')
.controller('PrintController', function($scope, $location, Persons, Storage, $filter) {
  var hash = /\/(\w+)$/.exec($location.absUrl()) || [];
  Storage.get({ hash: hash[1] }, function(data) {
    $scope.info = data;
    var number   = 0,
        prevSpec = 0;
    $scope.info.persons.forEach(function(e) {
      if(prevSpec != e.speciality) {
        number = 0;
      }
      prevSpec = e.speciality;
      e.number = ++number;

      e.$$initial = angular.copy(e);
    });
  });

  $scope.getYear = function() {
    return new Date().getFullYear() % 100;
  }
  $scope.getFullYear = function() {
    return new Date().getFullYear();
  }
  $scope.getDate = function() {
    return new Date();
  }

  $scope.getSpecialityShortName = function(name) {
    switch(name) {
      case 'програмна інженерія':
        return 'СП';
      case 'екологія, охорона навколишнього середовища та збалансоване природокористування':
        return 'ОНС';
      case 'охорона праці':
        return 'ОП';
      case 'автоматизація та комп’ютерно-інтегровані технології':
        return 'АТП';
      case 'гідроенергетика':
        return 'ГЕ';
      case 'менеджмент':
        return 'М';
      case 'теплоенергетика':
        return 'ТЕ';
      case 'електротехніка та електротехнології':
        return 'ЕТ';
      case 'облік і аудит':
        return 'ОА';
      case 'будівництво':
        return 'БУД';
      case 'гідротехніка':
        return 'ВР';
      case 'економіка підприємства':
        return 'ЕП';
      case 'економічна кібернетика':
        return 'ЕК';
      case 'електронні пристрої та системи':
        return 'ЕС';
      case 'машинобудування':
        return 'МБ';
      case 'металургія':
        return 'МЕТ';
      case 'мікро- та наноелектроніка':
        return 'МН';
      case 'фінанси і кредит':
        return 'Ф';
      case 'менеджмент організацій і адміністрування':
        return 'М';
      case 'управління проектами':
        return 'УП';
      case 'водопостачання та водовідведення':
        return 'ВВ';
      case 'металургійне обладнання':
        return 'МО';
      case 'металургія кольорових металів':
        return 'МКМ';
      case 'металургія чорних металів':
        return 'МЧМ';
      case 'обробка металів тиском':
        return 'ОМТ';
      case 'прикладна екологія та збалансоване природокористування':
        return 'ОНС';
      case 'міське будівництво та господарство':
        return 'МБГ';
      case 'промислове і цивільне будівництво':
        return 'ПЦБ';
    }

    console.log(new Error('Неможливо знайти напрям(спец.) "'+ name + '"'));
  };

  $scope.isBachelor = function(person) {
    return /бакалавр/.test(person.order.qualification);
  };

  $scope.save = function(person) {
    Persons.update(person, function(updated) {
      person.$$initial = angular.copy(updated);
    });
  };
  $scope.restore = function(person) {
    for(var property in person.$$initial) {
      if(property !== 'order') {
        person[property] = person.$$initial[property];
      }
    }
  };
});

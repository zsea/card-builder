'use strict';

angular
.module('ZSEACardBuilder.controllers')
.controller('PersonsController', function($scope, Persons, SelectedPersons, SelectedOrders) {
  $scope.$watch(SelectedOrders.get, updatePersons);

  $scope.$on('persons:selectall', function() {
    $scope.selectAll();
  });

  $scope.$on('persons:unselectall', function() {
    $scope.unselectAll();
  });

  $scope.$on('persons:sort', function() {
    $scope.sort();
  });

  $scope.select = function(person) {
    person.selected = !person.selected;
    if(!person.selected) {
      return SelectedPersons.removeOne(person);
    }

    SelectedPersons.addOne(person);
    if(!person.sex && !person.isLoading) {
      $scope.$parent.pendingRequests++;
      $scope.$parent.totalRequests++;

      person.isLoading = true;
      person.$info(function() {
        --$scope.$parent.pendingRequests;

        person.selected = true;
      });
    }
  }

  $scope.selectAll = function() {
    SelectedPersons.set($scope.persons);

    $scope.persons.forEach(function(person) {
      person.selected = false;
      $scope.select(person);
    });
  }

  $scope.unselectAll = function() {
    $scope.persons.forEach(function(person) { person.selected = false; });
    SelectedPersons.clear();
  }

  function updatePersons() {
    if(!SelectedOrders.get().length) {
      return;
    }

    $scope.$parent.pendingRequests = 0;
    $scope.$parent.totalRequests   = 0;
    $scope.persons = [];

    SelectedOrders.get().forEach(function(order, i) {
      Persons.query({ orderId: order._id }, function(persons) {
        order.loaded = true;
        persons.forEach(function(person) { person.order = order; });

        $scope.persons = $scope.persons.concat(persons);
      });
    });
  }

  $scope.sort = function() {
    $scope.persons.sort(function(lhs, rhs) {
      if(lhs.speciality && rhs.speciality) {
        var spec = lhs.speciality.localeCompare(rhs.speciality);

        if(spec !== 0) {
          return spec;
        }
      }
      return lhs.fio.localeCompare(rhs.fio);
    });
    SelectedPersons.set($scope.persons.filter(function(e) {
      return e.selected;
    }));
  };
});

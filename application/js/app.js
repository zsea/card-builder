'use strict';

angular.module('ZSEACardBuilder.directives',  []);
angular.module('ZSEACardBuilder.services',    []);
angular.module('ZSEACardBuilder.controllers', [])

angular.module('ZSEACardBuilder', [
  'ZSEACardBuilder.services',
  'ZSEACardBuilder.directives',
  'ZSEACardBuilder.controllers',

  'ngResource'
]);

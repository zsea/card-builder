'use strict';

angular
.module('ZSEACardBuilder.directives')
.directive('enrolmentCourse', function() {
  var decription = {
    1: 'першого',
    2: 'другого',
    3: 'третього',
    4: 'четвертого',
    5: 'п’ятого',
    6: 'шостого'
  };

  return {
    restrict: 'A',
    scope: {
      enrolmentCourse: '='
    },

    link: function($scope, element) {
      element.text(decription[$scope.enrolmentCourse]);
    }
  };
});

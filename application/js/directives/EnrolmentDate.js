'use strict';

angular
.module('ZSEACardBuilder.directives')
.directive('enrolmentDate', function() {
  var months = {
    0: 'січня',
    1: 'лютого',
    2: 'березня',
    3: 'квітня',
    4: 'травня',
    5: 'червня',
    6: 'липня',
    7: 'серпня',
    8: 'вересня',
    9: 'жовтня',
    10: 'листопада',
    11: 'грудня'
  };

  return {
    restrict: 'A',
    scope: {
      enrolmentDate: '='
    },
    template: '„{{ day }}” „{{ month }}” {{ year }} року',

    link: function($scope) {
      $scope.enrolmentDate = new Date($scope.enrolmentDate);

      $scope.getDay = function() {
        var date = $scope.enrolmentDate.getDate() + '';
        return date.length == 1 ? '0' + date : date;
      };

      $scope.getMonth = function() {
        var month = $scope.enrolmentDate.getMonth();
        return months[month];
      };


      $scope.day = $scope.getDay();
      $scope.month = $scope.getMonth();
      $scope.year = $scope.enrolmentDate.getFullYear();
    }
  };
});

'use strict';

angular
.module('ZSEACardBuilder.directives')
.directive('enrolmentForm', function() {
  var decription = {
    'денна': 'денної',
    'заочна': 'заочної'
  };

  return {
    restrict: 'A',
    scope: {
      enrolmentForm: '='
    },

    link: function($scope, element) {
      element.text(decription[$scope.enrolmentForm]);
    }
  };
});

'use strict';

angular
.module('ZSEACardBuilder.directives')
.directive('orderPayment', function() {
  return {
    restrict: 'C',
    scope: {
      order: '='
    },

    link: function($scope, elem) {
      $scope.check = function() {
        if($scope.order && $scope.order.payment === 'бюджет') {
          elem.addClass('budget');
        }
      }

      if(!$scope.order) {
        $scope.$watch('order', function() { $scope.check(); });
      }
      else {
        $scope.check();
      }
    }
  };
});

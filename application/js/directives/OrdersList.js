'use strict';

angular
.module('ZSEACardBuilder.directives')
.directive('ordersList', function($filter) {
  return {
    restrict: 'A',

    link: function($scope, elem) {
      $scope.$watch('[orders, searchOrder]', function(prev, next) {
        $scope.update();
      }, true);

      $scope.update = function() {
        $scope.error = false;
        $scope.clear();

        var orders = $filter('filter')($scope.orders, $scope.searchOrder);

        if(!orders.length) {
          $scope.error = true;
          return;
        }

        var idxs = [0];
        for(var i = 1; i < orders.length; ++i) {
          var current = orders[i],
              prev    = orders[i - 1];

          if(current.qualification !== prev.qualification) {
            idxs.push(i);
          }
        }

        idxs.reverse();
        idxs.forEach(function(index) {
          var qualification = $scope.trim(orders[index].qualification),
              anchor = elem.find('li:nth-child(' + (index + 1) + ')'),
              header = angular.element('<li>')
                        .addClass('group-header')
                        .text(qualification);

          header.insertBefore(anchor);
        });
      };

      $scope.clear = function() {
        elem.find('li.group-header').remove();
      };

      $scope.trim = function(qualification) {
        if(qualification.indexOf(',') !== -1) {
          var chunks = /([а-яії]+).+\([^,]+(.+)\)/g.exec(qualification);
          if(chunks) {
            return chunks[1] + chunks[2];
          }
        }

        return qualification;
      }
    }
  };
});

'use strict';

angular
.module('ZSEACardBuilder.directives')
.directive('personsViewBody', function() {
  return {
    restrict: 'A',

    link: function($scope, elem) {
      $scope.$watch('persons', function(prev, next) {
        $scope.update();
      }, true);

      $scope.update = function() {
        $scope.clear();
        if(!$scope.persons || $scope.persons.length < 2) {
          return;
        }

        var idxs = [];
        for(var i = 0; i < $scope.persons.length - 1; ++i) {
          var current = $scope.persons[i],
              next    = $scope.persons[i + 1];

          if(current.speciality !== next.speciality) {
            idxs.push(i);
          }
        }

        idxs.forEach(function(index) {
          elem.find('tr:nth-child(' + (index + 1) + ')').addClass('divider');
        });
      };

      $scope.clear = function() {
        elem.find('tr.divider').removeClass('divider');
      };
    }
  };
});

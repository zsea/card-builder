'use strict';

angular
.module('ZSEACardBuilder.directives')
.directive('enrolmentPayment', function() {
  var decription = {
    'контракт': 'кошти фізичних або юридичних осіб',
    'бюджет': 'державним замовленням',
  };

  return {
    restrict: 'A',
    scope: {
      enrolmentPayment: '='
    },

    link: function($scope, element) {
      element.text(decription[$scope.enrolmentPayment]);
    }
  };
});

'use strict';
require('coffee-script/register');

var express = require('express'),
    path    = require('path');

var app = express();

app.use('/', require('serve-static')('build'));
app.use('/', require('body-parser').json({ limit: '10mb' })) ;

app.route('/api').get(function(req, res, next) {
  res.header('Content-Type', 'appication/json; charset=utf-8');
  res.header('Access-Control-Allow-Origin', '*');

  next();
});

app.route('/').get(function(req, res, next) {
  res.sendFile(path.resolve('build/index.html'));
});
app.route('/print/:hash').get(function(req, res, next) {
  res.sendFile(path.resolve('build/print.html'));
});
app.route('/diff').get(function(req, res, next) {
  res.sendFile(path.resolve('build/diff.html'));
});

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/card-builder');

app.use('/api/orders',  require('./routes/order'));
app.use('/api/persons', require('./routes/persons'));
app.use('/api/storage', require('./routes/storage'));
app.use('/api/diff',    require('./routes/person-diff'));

var PORT = 8081;

console.log('Cards: server is running now!');
console.info('http://localhost:' + PORT);

app.listen(PORT);
